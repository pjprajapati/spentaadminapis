<?php
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'); 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');

// get database connection
include_once '../config/database.php';
include_once '../objects/towers.php';

// instantiate user object
include_once '../objects/user.php';

$database = new Database();
$db = $database->getConnection();
 
$request_method = $_SERVER['REQUEST_METHOD'];
if($request_method != 'POST'){
    $user_arr=array(
        "status" => false,
        "error_code" => '0',
        "message" => $request_method. " method not available!"
    );
    print_r(json_encode($user_arr));
    exit;
}

$json = file_get_contents('php://input');
$data = json_decode($json, true);

if(!empty($data['project_id']))
{
    $projectId = $data['project_id'];
    
    
    $datatime = date("Y-m-d H:i:s");
    
    if(isset($data['image']) && isset($data['preBookingAmount'])){
        $images = $data['image'];
        $preBookingAmount = $data['preBookingAmount'];
        $query = "UPDATE Projects SET `image`='".$images."', `preBookingAmount`='".$preBookingAmount."', updatedAt='".$datatime."' WHERE id = '".$projectId."'";
        $stmt = $db->prepare($query);
        $stmt->execute();
        $msg = "Project updated successfully!";
    }else if(isset($data['image']) && !isset($data['preBookingAmount'])){
        $images = $data['image'];
        $query = "UPDATE Projects SET `image`='".$image."', updatedAt='".$datatime."' WHERE id = '".$projectId."'";
        $stmt = $db->prepare($query);
        $stmt->execute();
        $msg = "You have successfully updated master plan image";
    }else if(!isset($data['image']) && isset($data['preBookingAmount'])){
        $preBookingAmount = $data['preBookingAmount'];
        $query = "UPDATE Projects SET  `preBookingAmount`='".$preBookingAmount."', updatedAt='".$datatime."' WHERE id = '".$projectId."'";
        $stmt = $db->prepare($query);
        $stmt->execute();
        $msg = "You have successfully updated booking amount";
    }

    if($stmt->rowCount() > 0){    
        $user_arr=array(
            "status" => true,
            "error_code" => '1',
            "message" => $msg
        );
    }else{
        $user_arr=array(
            "status" => true,
            "error_code" => '0',
            "message" => "Record not update."
        );
    }
}else{
    $user_arr=array(
        "status" => false,
        "error_code" => '-11',
        "message" => "Required parameter missing!",
    );
}

$db = null;
header('Content-Type: application/json');
print_r(json_encode($user_arr));
?>