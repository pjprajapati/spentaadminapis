<?php
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'); 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');

// get database connection
include_once '../config/database.php';
include_once '../objects/towers.php';

// instantiate user object
include_once '../objects/user.php';

$database = new Database();
$db = $database->getConnection();
 
$request_method = $_SERVER['REQUEST_METHOD'];
if($request_method != 'POST'){
    $user_arr=array(
        "status" => false,
        "error_code" => '0',
        "message" => $request_method. " method not available!"
    );
    print_r(json_encode($user_arr));
    exit;
}

$json = file_get_contents('php://input');
$data = json_decode($json, true);

if(!empty($data['name']) && !empty($data['project_id']) && !empty($data['image']) && !empty($data['totalFloors']) && !empty($data['reraNumber']))
{
    if(isset($data['points'])){
        $points = $data['points'];
    }else{
        $points = "";
    }

    if(isset($data['id'])){
        $id = $data['id'];
    }else{
        $id = "";
    }

    $projectId = $data['project_id'];
    $typologyId = "";//$data['typologyId'];
    $name = $data['name'];
    $image = $data['image'];
    $totalFloors = $data['totalFloors'];
    $status = "1";
    $reraNumber = $data['reraNumber'];

    $ticketSizeMin = $data['ticketSizeMin'];
    $ticketSizeMax = $data['ticketSizeMax'];

    $datatime = date("Y-m-d H:i:s");

    if(empty($id)){

        $query = "INSERT INTO Towers SET `name`='".$name."', `status`='".$status."', `images`='".$image."', `ticketSizeMin`='".$ticketSizeMin."', `ticketSizeMax`='".$ticketSizeMax."', totalFloors='".$totalFloors."', createdAt='".$datatime."', updatedAt='".$datatime."'";
        // prepare query
        $stmt = $db->prepare($query);
        $stmt->execute();
        $towerId = $db->lastInsertId();

        if($towerId > 0){
            /* foreach($typologyId as $r){
                $t_id = $r;
                $subinsert = "INSERT INTO ProjectTowerMappings SET projectId='".$projectId."', `typologyId`='".$t_id."', `status`='".$status."', reraNumber='".$reraNumber."', towerId='".$towerId."', createdAt='".$datatime."', updatedAt='".$datatime."'";
                // prepare query
                $stmt = $db->prepare($subinsert);
                $stmt->execute();
                $id = $db->lastInsertId();
            } */

            $subinsert = "INSERT INTO ProjectTowerMappings SET projectId='".$projectId."', `status`='".$status."', reraNumber='".$reraNumber."', towerId='".$towerId."', createdAt='".$datatime."', updatedAt='".$datatime."'";
            // prepare query
            $stmt = $db->prepare($subinsert);
            $stmt->execute();
            $id = $db->lastInsertId();

            for ($x = 1; $x <= $totalFloors; $x++) {
                $checkfloor = "SELECT number,id FROM Floors WHERE number = '".$x."'";
                $stmt_q = $db->prepare($checkfloor);
                $stmt_q->execute();                
                if($stmt_q->rowCount() < 0){
                    $floorsinsert = "INSERT INTO Floors SET `number`='".$x."', `status`='".$status."', createdAt='".$datatime."', updatedAt='".$datatime."'";
                    // prepare query
                    $stmt = $db->prepare($floorsinsert);
                    $stmt->execute();
                    $floorId = $db->lastInsertId();
                }else{
                    $row = $stmt_q->fetchAll(PDO::FETCH_ASSOC);
                    $floorId = $row[0]['id'];
                }

                $floorsinsert = "INSERT INTO TowerFloorMappings SET `towerId`='".$towerId."', floorId = '".$floorId."', `status`='".$status."', createdAt='".$datatime."', updatedAt='".$datatime."'";
                // prepare query
                $stmt = $db->prepare($floorsinsert);
                $stmt->execute();
            }
        }

    }else{

        $checktower = "SELECT totalFloors FROM Towers WHERE id = '".$id."'";
        $stmt_2 = $db->prepare($checktower);
        $stmt_2->execute();  
        if($stmt_2->rowCount() > 0){
            $row = $stmt_2->fetchAll(PDO::FETCH_ASSOC);
            $oldtotalFloors = $row[0]['totalFloors'];
        }else{
            $oldtotalFloors = "0";
        }

        $query = "UPDATE Towers SET `name`='".$name."', `status`='".$status."', `images`='".$image."', `ticketSizeMin`='".$ticketSizeMin."', `ticketSizeMax`='".$ticketSizeMax."', totalFloors='".$totalFloors."', updatedAt='".$datatime."' WHERE id = '".$id."'";
        // prepare query
        $stmt = $db->prepare($query);
        $stmt->execute();

        /* foreach($typologyId as $r){
            $t_id = $r;

            $checkfloor = "SELECT id FROM ProjectTowerMappings WHERE projectId='".$projectId."' AND `typologyId`='".$t_id."' AND towerId='".$id."'";
            $stmt_q = $db->prepare($checkfloor);
            $stmt_q->execute();                
            if($stmt_q->rowCount() < 0){
                $subinsert = "INSERT INTO ProjectTowerMappings SET projectId='".$projectId."', `typologyId`='".$t_id."', `status`='".$status."', reraNumber='".$reraNumber."', towerId='".$towerId."', createdAt='".$datatime."', updatedAt='".$datatime."'";
                $stmt = $db->prepare($subinsert);
                $stmt->execute();
                $floorId = $db->lastInsertId();
            }
        } */

        $checkfloor = "SELECT id FROM ProjectTowerMappings WHERE projectId='".$projectId."' AND towerId='".$id."'";
        $stmt_q = $db->prepare($checkfloor);
        $stmt_q->execute();                
        if($stmt_q->rowCount() < 0){
            $subinsert = "INSERT INTO ProjectTowerMappings SET projectId='".$projectId."', `status`='".$status."', reraNumber='".$reraNumber."', towerId='".$towerId."', createdAt='".$datatime."', updatedAt='".$datatime."'";
            $stmt = $db->prepare($subinsert);
            $stmt->execute();
            $floorId = $db->lastInsertId();
        }else{
            $ptm = $stmt_q->fetchAll(PDO::FETCH_ASSOC);
            $ptmId = $ptm[0]['id'];
            $subinsert = "UPDATE ProjectTowerMappings SET reraNumber='".$reraNumber."', updatedAt='".$datatime."' WHERE projectId='".$projectId."' AND towerId='".$id."'";
            $stmt = $db->prepare($subinsert);
            $stmt->execute();
        }

        $diff = $totalFloors-$oldtotalFloors;
        if($diff < 0){
            $diff_count = str_ireplace("-","",$diff);
            $floorsinsert = "DELETE FROM TowerFloorMappings WHERE towerId = '".$towerId."' AND id IN (SELECT TOP $diff_count id FROM TowerFloorMappings ORDER BY id DESC)";
            // prepare query
            $stmt = $db->prepare($floorsinsert);
            $stmt->execute(); 
        }
        
        for ($x = 1; $x <= $totalFloors; $x++) {
            $checkfloor = "SELECT id,number FROM Floors WHERE number = '".$x."'";
            $stmt_q = $db->prepare($checkfloor);
            $stmt_q->execute();                
            if($stmt_q->rowCount() <= 0){
                $floorsinsert = "INSERT INTO Floors SET `number`='".$x."', `status`='".$status."', createdAt='".$datatime."', updatedAt='".$datatime."'";
                // prepare query
                $stmt = $db->prepare($floorsinsert);
                $stmt->execute();
                $floorId = $db->lastInsertId();
            }else{
                $row = $stmt_q->fetchAll(PDO::FETCH_ASSOC);
                $floorId = $row[0]['id'];
            }

            $check = "SELECT id FROM TowerFloorMappings WHERE towerId = '".$id."' AND floorId = '".$floorId."'";
            $stmt_1 = $db->prepare($check);
            $stmt_1->execute();
            if($stmt_1->rowCount() <= 0){
                $floorsinsert = "INSERT INTO TowerFloorMappings SET `towerId`='".$id."', floorId = '".$floorId."', `status`='".$status."', createdAt='".$datatime."', updatedAt='".$datatime."'";
                // prepare query
                $stmt = $db->prepare($floorsinsert);
                $stmt->execute();
            }
        }
    }

    if(!empty($towerId)){
        $user_arr=array(
            "status" => true,
            "error_code" => '1',
            "message" => "Tower added successfully!"
        );
    }else{
        $user_arr=array(
            "status" => true,
            "error_code" => '1',
            "message" => "Tower updated successfully!"
        );
    }
}else{
    $user_arr=array(
        "status" => false,
        "error_code" => '-11',
        "message" => "Required parameter missing!",
    );
}

$db = null;
header('Content-Type: application/json');
print_r(json_encode($user_arr));
?>