<?php
// include database and object files
include_once '../config/database.php';
include_once '../objects/user.php';
include_once '../objects/projects.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
$request_method = $_SERVER['REQUEST_METHOD'];
if($request_method != 'POST'){
    $user_arr=array(
        "status" => false,
        "error_code" => '0',
        "message" => $request_method. " method not available!"
    );
    print_r(json_encode($user_arr));
    exit;
}

$json = file_get_contents('php://input');
$data = json_decode($json, true);

if(!empty($data['email']) && !empty($data['password']))
{
    // prepare user object
    $user = new User($db);
    // set ID property of user to be edited
    $user->email = isset($data['email']) ? $data['email'] : die();
    $user->password = isset($data['password']) ? $data['password'] : die();
    // read the details of user to be edited
    $stmt = $user->login();
    if($stmt->rowCount() > 0){
        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $project_id = $row['project_id'];

        /* $project = new Projects($db);
        $project->project_id = $project_id;
        $qry = $project->getDataById();
        $project_details = $qry->fetch(PDO::FETCH_ASSOC);
        
        $response = array();
        $d['user_data'] = $row;
        $d['project_data'] = $project_details;
        $response[] = $d; */
        
        // create array
        $user_arr=array(
            "status" => true,
            "error_code" => '1',
            "message" => "Successfully Signin!",
            "data" => $row
        );
    }else{
        $user_arr=array(
            "status" => false,
            "error_code" => '0',
            "message" => "Invalid Email or Password!",
        );
    }
}else{
    $user_arr=array(
        "status" => false,
        "error_code" => '-11',
        "message" => "Required parameter missing!",
    );
}
$db = null;
header('Content-Type: application/json');
// make it json format
print_r(json_encode($user_arr));
?>