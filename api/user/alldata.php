<?php
// include database and object files
include_once '../config/database.php';
include_once '../objects/user.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
$request_method = $_SERVER['REQUEST_METHOD'];
if($request_method != 'POST'){
    $user_arr=array(
        "status" => false,
        "error_code" => '0',
        "message" => $request_method. " method not available!"
    );
    print_r(json_encode($user_arr));
    exit;
}

$page_rows = 10;

$json = file_get_contents('php://input');
$data = json_decode($json, true);

if(!empty($data['pagesize']))
{
    $page_rows = $data['pagesize'];
    $pagenum = $data['rowoffset'];

    if(isset($data['email'])){
        $email = $data['email'];
    }else{
        $email = "";
    }

    if(isset($data['client_name'])){
        $client_name = $data['client_name'];
    }else{
        $client_name = "";
    }

    if(isset($data['datetimefilter'])){
        $datetimefilter = $data['datetimefilter'];
        if(!empty($datetimefilter)){
            $startdate = $datetimefilter['startdate'];
            $enddate = $datetimefilter['enddate'];
        }else{
            $startdate = "";
            $enddate = "";
        }
    }else{
        $startdate = "";
        $enddate = "";
    }

    $usersall = new User($db);
    $usersall->userid = $email;
    $usersall->client_name = $client_name;
    $usersall->startdate = $startdate;
    $usersall->enddate = $enddate;
    $stmt1 = $usersall->getAllData();
    $rows = $stmt1->rowCount();
    
    // prepare user object
    $users = new User($db);
    $users->page_rows = $page_rows;
    $users->startPage = $pagenum;
    $users->userid = $email;
    $users->client_name = $client_name;
    $users->startdate = $startdate;
    $users->enddate = $enddate;
    $stmt = $users->getAllDataWithPaging();
    
    if($stmt->rowCount() > 0){
        // get retrieved row
        $row = $stmt->fetchAll();
        
        $response = array();
        foreach($row as $val){
            $d['userid'] = $val['userid'];
            $d['firstname'] = $val['firstname'];
            $d['lastname'] = $val['lastname'];
            $d['usertype'] = $val['usertype'];
            $d['project'] = $val['project'];
            $d['config'] = $val['config'];
            $d['sector'] = $val['sector'];
            $d['status'] = $val['status'];
            $config = $val['config'];
            $config_arr = json_decode($config, true);
            //print_r($config_arr);
            $clients = $config_arr['clients'];
            $client_arr = $clients[0];
            $client_name = $client_arr['client'];
            $project_name = $client_arr['projects'][0];
            $d['client_name'] = $client_name;
            $d['project_name'] = $project_name;
            $d['username'] = $config_arr['loggedInUser'];
            $d['username'] = $config_arr['loggedInUser'];
            if(isset($config_arr['senderemail'])){
                $d['senderemail'] = $config_arr['senderemail'];
            }else{
                $d['senderemail'] = '';
            }
            $fileDetails = $config_arr['fileDetails'];
            
            $d['maxfilesize'] = $fileDetails['maxfilesize'];
            $response[] = $d;
        }

        $user_arr=array(
            "status" => true,
            "error_code" => '1',
            "message" => "List of user data",
            "data" => $response,
            "total_record" => $rows
        );
    }
    else{
        $user_arr=array(
            "status" => false,
            "error_code" => '0',
            "message" => "Record not found!",
        );
    }
}else{
    $user_arr=array(
        "status" => false,
        "error_code" => '-11',
        "message" => "Required parameter missing!",
    );
}
// make it json format
print_r(json_encode($user_arr));
?>