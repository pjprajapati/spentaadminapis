<?php
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'); 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');

// include database and object files
include_once '../config/database.php';

$request_method = $_SERVER['REQUEST_METHOD'];
if($request_method != 'POST'){
    $user_arr=array(
        "status" => false,
        "error_code" => '0',
        "message" => $request_method. " method not available!"
    );
    print_r(json_encode($user_arr));
    exit;
}

$json = file_get_contents('php://input');
$data = json_decode($json, true);

if(!empty($data['project_id']) && !empty($data['towerId']))
{
    $project_id = $data['project_id'];
    $towerId = $data['towerId'];
    if(isset($data['floorId'])){
        $floorId = $data['floorId'];
    }else{
        $floorId = "";
    }

    if(isset($data['typologyId'])){
        $typologyId = $data['typologyId'];
    }else{
        $typologyId = "";
    }

    // get database connection
    $database = new Database();
    $db = $database->getConnection();
    
    if(!empty($towerId) && !empty($floorId) && empty($typologyId)){
        $query = "SELECT tffm.id, tfm.floorId, tfm.towerId, tffm.flatId, f.name as flatName, tffm.typologyId, tffm.sqftSize, tffm.unit, tffm.price, tffm.soldStatus FROM Flats as f LEFT JOIN TowerFloorFlatMappings as tffm ON f.id = tffm.flatId LEFT JOIN TowerFloorMappings as tfm ON tffm.towerFloorMappingId = tfm.id WHERE tfm.towerId='".$towerId."' AND tfm.floorId='".$floorId."'";
    }else if(!empty($towerId) && !empty($typologyId)){
        $query = "SELECT tffm.id, tfm.floorId, tfm.towerId, tffm.flatId, f.name as flatName, tffm.typologyId, tffm.sqftSize, tffm.unit, tffm.price, tffm.soldStatus FROM Flats as f LEFT JOIN TowerFloorFlatMappings as tffm ON f.id = tffm.flatId LEFT JOIN TowerFloorMappings as tfm ON tffm.towerFloorMappingId = tfm.id WHERE tfm.towerId='".$towerId."' AND tffm.typologyId='".$typologyId."'";
    }else{
        $query = "SELECT tffm.id, tfm.floorId, tfm.towerId, tffm.flatId, f.name as flatName, tffm.typologyId, tffm.sqftSize, tffm.unit, tffm.price, tffm.soldStatus FROM Flats as f LEFT JOIN TowerFloorFlatMappings as tffm ON f.id = tffm.flatId LEFT JOIN TowerFloorMappings as tfm ON tffm.towerFloorMappingId = tfm.id WHERE tfm.towerId='".$towerId."'";
    }
    $stmt = $db->prepare($query);
    $stmt->execute();
    
    if($stmt->rowCount() > 0){
        // get retrieved row
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $user_arr=array(
            "status" => true,
            "error_code" => '1',
            "message" => "Flat lists",
            "data" => $row
        );
    }
    else{
        $user_arr=array(
            "status" => false,
            "error_code" => '0',
            "message" => "Record not found!",
        );
    }
}else{
    $user_arr=array(
        "status" => false,
        "error_code" => '-11',
        "message" => "Required parameter missing!",
    );
}
$db = null;
header('Content-Type: application/json');
// make it json format
print_r(json_encode($user_arr));
?>