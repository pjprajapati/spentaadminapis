<?php
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'); 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');

// get database connection
include_once '../config/database.php';
include_once '../objects/towers.php';

// instantiate user object
include_once '../objects/user.php';

$database = new Database();
$db = $database->getConnection();
 
$request_method = $_SERVER['REQUEST_METHOD'];
if($request_method != 'POST'){
    $user_arr=array(
        "status" => false,
        "error_code" => '0',
        "message" => $request_method. " method not available!"
    );
    print_r(json_encode($user_arr));
    exit;
}

$json = file_get_contents('php://input');
$data = json_decode($json, true);

if(!empty($data['project_id']) && !empty($data['charges']) && !empty($data['value']))
{
    if(isset($data['id'])){
        $id = $data['id'];
    }else{
        $id = "";
    }

    $projectId = $data['project_id'];
    $charges = $data['charges'];
    $value = $data['value'];
    $datatime = date("Y-m-d H:i:s");

    if(empty($id)){

        $query = "INSERT INTO OtherCharges SET `projectId`='".$projectId."', `charges`='".$charges."', `value`='".$value."', `status` = '1', createdAt='".$datatime."', updatedAt='".$datatime."'";
        // prepare query
        $stmt = $db->prepare($query);
        $stmt->execute();
        $otherchargesId = $db->lastInsertId();
    }else{
        $query = "UPDATE OtherCharges SET  `charges`='".$charges."', `value`='".$value."', `status` = '1', updatedAt='".$datatime."' WHERE id = '".$id."'";
        // prepare query
        $stmt = $db->prepare($query);
        $stmt->execute();
        $otherchargesId = $id;
    }

    if(empty($id)){
        $user_arr=array(
            "status" => true,
            "error_code" => '1',
            "message" => "Charges added successfully!"
        );
    }else{
        $user_arr=array(
            "status" => true,
            "error_code" => '1',
            "message" => "Charges updated successfully!"
        );
    }
}else{
    $user_arr=array(
        "status" => false,
        "error_code" => '-11',
        "message" => "Required parameter missing!",
    );
}

$db = null;
header('Content-Type: application/json');
print_r(json_encode($user_arr));
?>