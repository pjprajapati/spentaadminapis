<?php
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'); 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');

// get database connection
include_once '../config/database.php';
 
// instantiate user object
include_once '../objects/user.php';
include_once '../objects/config.php';

$database = new Database();
$db = $database->getConnection();
 
$request_method = $_SERVER['REQUEST_METHOD'];
if($request_method != 'POST'){
    $user_arr=array(
        "status" => false,
        "error_code" => '0',
        "message" => $request_method. " method not available!"
    );
    print_r(json_encode($user_arr));
    exit;
}

$json = file_get_contents('php://input');
$data = json_decode($json, true);

if(!empty($data['userid']) && ($data['status'] == 'Active' || $data['status'] == 'Inactive' ) )
{
    $user = new User($db);
    
    $userid = $data['userid'];
    $status = $data['status'];
    
    $query = "UPDATE Users SET status=:status WHERE userid=:userid";    
    // prepare query
    $stmt = $db->prepare($query);

    // bind values
    $stmt->bindParam(":userid", $userid);
    $stmt->bindParam(":status", $status);
    $stmt->execute();
       
    $user_arr=array(
        "status" => true,
        "error_code" => '1',
        "message" => "Status changes successfully!"
    );
}else{
    $user_arr=array(
        "status" => false,
        "error_code" => '-11',
        "message" => "Required parameter missing!",
    );
}

print_r(json_encode($user_arr));
?>