<?php
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'); 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');

// include database and object files
include_once '../config/database.php';
include_once '../objects/towers.php';

$request_method = $_SERVER['REQUEST_METHOD'];
if($request_method != 'POST'){
    $user_arr=array(
        "status" => false,
        "error_code" => '0',
        "message" => $request_method. " method not available!"
    );
    print_r(json_encode($user_arr));
    exit;
}


$json = file_get_contents('php://input');
$data = json_decode($json, true);

if(!empty($data['project_id']))
{
    $id = $data['project_id'];

    // get database connection
    $database = new Database();
    $db = $database->getConnection();

    // prepare user object
    $project = new Towers($db);
    $project->project_id = $id;
    $stmt = $project->getDataByProjectId();
    if($stmt->rowCount() > 0){
        // get retrieved row
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        /* print_r($row); */
        $user_arr=array(
            "status" => true,
            "error_code" => '1',
            "message" => "Tower details",
            "data" => $row
        );
    }
    else{
        $user_arr=array(
            "status" => false,
            "error_code" => '0',
            "message" => "Record not found!",
        );
    }
}else{
    $user_arr=array(
        "status" => false,
        "error_code" => '-11',
        "message" => "Required parameter missing!",
    );
}
$db = null;
header('Content-Type: application/json');
// make it json format
print_r(json_encode($user_arr));
?>