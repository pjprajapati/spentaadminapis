<?php
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'); 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');

// include database and object files
include_once '../config/database.php';
include_once '../objects/config.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare user object
$config = new Config($db);
$stmt = $config->getAllData();
if($stmt->rowCount() > 0){
    // get retrieved row
    $row = $stmt->fetchAll();
   
    $response = array();
    foreach($row as $val){
        $d['config_type_id'] = $val['id'];
        $d['type'] = $val['type'];
        $response[] = $d;
    }

    $user_arr=array(
        "status" => true,
        "error_code" => '1',
        "message" => "List of config type",
        "data" => $response
    );
}
else{
    $user_arr=array(
        "status" => false,
        "error_code" => '0',
        "message" => "Record not found!",
    );
}
// make it json format
print_r(json_encode($user_arr));
?>