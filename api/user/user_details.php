<?php
// include database and object files
include_once '../config/database.php';
include_once '../objects/user.php';
include_once '../objects/config.php';

// get database connection
$database = new Database();
$db = $database->getConnection();
 
$request_method = $_SERVER['REQUEST_METHOD'];
if($request_method != 'POST'){
    $user_arr=array(
        "status" => false,
        "error_code" => '0',
        "message" => $request_method. " method not available!"
    );
    print_r(json_encode($user_arr));
    exit;
}

$json = file_get_contents('php://input');
$data = json_decode($json, true);

if(!empty($data['userid']))
{
    $userid = $data['userid'];

    // prepare user object
    $users = new User($db);
    $users->userid = $userid;
    $stmt = $users->getUserDetails();
    
    if($stmt->rowCount() > 0){
        // get retrieved row
        $row = $stmt->fetchAll();
        
        $response = array();
        foreach($row as $val){
            $d['userid'] = $val['userid'];
            $d['firstname'] = $val['firstname'];
            $d['lastname'] = $val['lastname'];
            $d['usertype'] = $val['usertype'];
            $d['project'] = $val['project'];
            $d['sector'] = $val['sector'];
            $d['password'] = $val['password'];
            $d['config'] = $val['config'];
            $config = $val['config'];
            $config_arr = json_decode($config, true);
            if(!empty($config))
            {   
                if (array_key_exists("module_access", $config_arr))
                {
                    $module_access = $config_arr['module_access'];
                }
                else
                {
                    $module_access = array("Funnel","Connect","Jedai","Acumen");
                }
            }else{
                $module_access = array("Funnel","Connect","Jedai","Acumen");
            }
            
            $clients = $config_arr['clients'];
            $client_arr = $clients[0];
            $client_name = $client_arr['client'];
            $project_name = $client_arr['projects'][0];
            $d['client_name'] = $client_name;
            $d['project_name'] = $project_name;
            $d['username'] = $config_arr['loggedInUser'];
            if(isset($config_arr['senderemail'])){
                $d['senderemail'] = $config_arr['senderemail'];
            }else{
                $d['senderemail'] = '';
            }
            $fileDetails = $config_arr['fileDetails'];
            //print_r($config_arr);
            $d['maxfilesize'] = $fileDetails['maxfilesize'];
            $module_access = $config_arr['module_access'];

            $tmp = new User($db);
            $tmp->userid = $userid;
            $tmp->type = "sms";
            $stmt1 = $tmp->getTempletes();
            $sms_row = $stmt1->fetchAll();
            $sms_response = array();
            foreach($sms_row as $val1){
                $r['templatename'] = $val1['templatename'];
                $r['smsbody'] = $val1['smsbody'];
                $sms_response[] = $r;
            }

            $tmp_email = new User($db);
            $tmp_email->userid = $userid;
            $tmp_email->type = "email";
            $stmt2 = $tmp_email->getTempletes();
            $email_row = $stmt2->fetchAll();
            $email_response = array();
            foreach($email_row as $val1){
                $r1['templatename'] = $val1['templatename'];
                $r1['email_subject'] = "";
                $r1['smsbody'] = $val1['smsbody'];
                $r1['image_url'] = $val1['thumbnail'];
                $email_response[] = $r1;
            }

            $d['sms_template'] = $sms_response;
            $d['email_template'] = $email_response;

            $config1 = new Config($db);
            $stmt1 = $config1->getColumnsName();
            $result = $stmt1->fetchAll(PDO::FETCH_ASSOC);

            $json_fields = array();
            foreach($result as $rec){
                $json_fields[] = $rec['COLUMN_NAME'];
            }
            $d['lead_fields'] = json_encode($json_fields);

            $userid = $data['userid'];
            $query = "SELECT * FROM credits WHERE userid = '".$userid."'";
            $stmt = $db->prepare($query);
            $stmt->execute();
            $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(!empty($row)){
                $d['sms_credit'] = $row[0]['sms'];
                $d['email_credit'] = $row[0]['email'];
            }else{
                $d['sms_credit'] = "";
                $d['email_credit'] = "";
            }
            $d['module_access'] = $module_access;
            $response[] = $d;
        }

        $user_arr=array(
            "status" => true,
            "error_code" => '1',
            "message" => "List of user data",
            "data" => $response
        );
    }
    else{
        $user_arr=array(
            "status" => false,
            "error_code" => '0',
            "message" => "Record not found!",
        );
    }
}else{
    $user_arr=array(
        "status" => false,
        "error_code" => '-11',
        "message" => "Required parameter missing!",
    );
}
// make it json format
print_r(json_encode($user_arr));
?>