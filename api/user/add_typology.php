<?php
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'); 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');

// get database connection
include_once '../config/database.php';
include_once '../objects/typologies.php';

// instantiate user object
include_once '../objects/user.php';

$database = new Database();
$db = $database->getConnection();
 
$request_method = $_SERVER['REQUEST_METHOD'];
if($request_method != 'POST'){
    $user_arr=array(
        "status" => false,
        "error_code" => '0',
        "message" => $request_method. " method not available!"
    );
    print_r(json_encode($user_arr));
    exit;
}

$json = file_get_contents('php://input');
$data = json_decode($json, true);

if(!empty($data['name']) && !empty($data['color']) && !empty($data['customField1']))
{
    $typologies = new Typologies($db);
    $typologies->name = $data['name'];
    $typologies->color = $data['color'];
    $typologies->status = "1";
    $typologies->customField1 = $data['customField1'];
    if(empty($data['id'])){
        $result = $typologies->insert();
    }else{
        $result = "";
    }
    // create the user
    if(!empty($result)){
        $subtypologies = new Typologies($db);
        $subtypologies->project_id = $data['project_id'];
        $subtypologies->id = $result;
        $subtypologies->customerId = "1";
        $subtypologies->status = "1";
        $subtypologies->insertsub();
    }else{
        $typologies->id = $data['id'];
        $typologies->update();

        $subtypologies = new Typologies($db);
        $subtypologies->project_id = $data['project_id'];
        $subtypologies->id = $data['id'];
        $subtypologies->customerId = "1";
        $subtypologies->status = "1";
        $subtypologies->updatesub();
    }

    if(!empty($result)){
        $user_arr=array(
            "status" => true,
            "error_code" => '1',
            "message" => "Typology added successfully!"
        );
    }else{
        $user_arr=array(
            "status" => true,
            "error_code" => '1',
            "message" => "Typology updated successfully!"
        );
    }
}else{
    $user_arr=array(
        "status" => false,
        "error_code" => '-11',
        "message" => "Required parameter missing!",
    );
}

$db = null;
header('Content-Type: application/json');
print_r(json_encode($user_arr));
?>