<?php
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'); 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');

// get database connection
include_once '../config/database.php';
include_once '../objects/towers.php';

// instantiate user object
include_once '../objects/user.php';

$database = new Database();
$db = $database->getConnection();
 
$request_method = $_SERVER['REQUEST_METHOD'];
if($request_method != 'POST'){
    $user_arr=array(
        "status" => false,
        "error_code" => '0',
        "message" => $request_method. " method not available!"
    );
    print_r(json_encode($user_arr));
    exit;
}

$json = file_get_contents('php://input');
$data = json_decode($json, true);

if(!empty($data['project_id']))
{   
    $projectId = $data['project_id'];
    $query = "SELECT id,projectId,charges,`value` FROM OtherCharges WHERE projectId='".$projectId."'";
    // prepare query statement
    $stmt = $db->prepare($query);
    // execute query
    $stmt->execute();
    if($stmt->rowCount() > 0){
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $user_arr=array(
            "status" => true,
            "error_code" => '1',
            "message" => "list of charges",
            "data" => $row
        );
    }else{
        $user_arr=array(
            "status" => true,
            "error_code" => '0',
            "message" => "Recort not found!"
        );
    }
}else{
    $user_arr=array(
        "status" => false,
        "error_code" => '-11',
        "message" => "Required parameter missing!",
    );
}

$db = null;
header('Content-Type: application/json');
print_r(json_encode($user_arr));
?>