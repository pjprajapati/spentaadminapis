<?php
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'); 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');

// get database connection
include_once '../config/database.php';
 
// instantiate user object
include_once '../objects/user.php';
include_once '../objects/config.php';

$database = new Database();
$db = $database->getConnection();
 
$request_method = $_SERVER['REQUEST_METHOD'];
if($request_method != 'POST'){
    $user_arr=array(
        "status" => false,
        "error_code" => '0',
        "message" => $request_method. " method not available!"
    );
    print_r(json_encode($user_arr));
    exit;
}

$json = file_get_contents('php://input');
$data = json_decode($json, true);

if(!empty($data['userid']) && !empty($data['username']) && !empty($data['client_name']) && !empty($data['usertype']))
{
    $user = new User($db);
    
    $configtype = $data['configtype'];
    $config1 = new Config($db);
    $config1->type = $configtype;
    $stmt1 = $config1->getDataByType();
    $result = $stmt1->fetchAll();
    $ConnectPlus = $result[0]['connect_plus'];
    $ConnectPlus = json_decode($ConnectPlus,true);
    //print_r($ConnectPlus);
    //print_r($data['config']);
    // set user property values
    $user->userid = $data['userid'];
    if(isset($data['project'])){
        $user->password = $data['password'];
    }else{
        $user->password = "";
    }
    $user->configtype = $configtype;
    $user->firstname = "";
    $user->lastname = "";
    $user->client_name = $data['client_name'];
    $user->usertype = $data['usertype'];
    if(isset($data['project'])){
        $user->project = $data['project'];
    }else{
        $user->project = "";
    }
    
    /* if(isset($data['config'])){
        $user->config = $data['config'];
    }else{
        $user->config = "";
    } */

    $username = $data['username'];
    $client_name = $data['client_name'];
    $project_name = $data['project_name'];
    $senderemail = $data['senderemail'];
    $maxfilesize = $data['maxfilesize'];
    $module_access = $data['module_access'];

    $clients = array();
    $d['client'] = $client_name;
    $project = array();
    $project[] = $project_name;
    $d['projects'] = $project;
    $clients[] = $d;
    $features = array("Import CSV","Send SMS","Send Email");
    
    $extensions = array();
    $extensions[] = ".csv";

    $fileDetails = array();
    $fileDetails['extensions'] = $extensions;
    $fileDetails['maxfilesize'] = $maxfilesize;
    
    $final = array();
    $final['clients'] = $clients;
    $final['features'] = $features;
    $final['fields'] = $data['config'];
    $final['ConnectPlus'] = $ConnectPlus;
    $final['loggedInUser'] = $username;
    $final['user'] = $data['userid'];
    $final['senderemail'] = $senderemail;
    $final['userType'] = $data['usertype'];
    $final['fileDetails'] = (object)$fileDetails;
    $final['module_access'] = $data['module_access'];
    $fianl_json = json_encode($final);
    $fianl_json = stripslashes($fianl_json);
    //$fianl_json = $final;
    $user->config = $fianl_json;

    // create the user
    if($user->signup()){
    }
    else{
        $user->userUpdate();
    }

    $userid = $data['userid'];
    $query1 = "DELETE FROM notification_templates WHERE userid = '".$userid."'";
    $stmt1 = $db->prepare($query1);
    $stmt1->execute();
    
    $email_template = $data['email_template'];
    foreach($email_template as $rec){
        $template_name = $rec['template_name'];
        $email_subject = $rec['email_subject'];
        $email_body = $rec['email_body'];
        $image_url = $rec['image_url'];

        $userid = $data['userid'];
        $query = "SELECT * FROM notification_templates WHERE userid = '".$userid."' AND templatename = '".$template_name."'";
        // prepare query statement
        $stmt = $db->prepare($query);
        // execute query
        $stmt->execute();
        
        if($stmt->rowCount() > 0){
            $query = "UPDATE notification_templates SET userid=:userid, thumbnail=:thumbnail, previewimage=:previewimage, type=:type, clientname=:clientname, project=:project, smsbody=:smsbody WHERE templatename=:templatename";    
            // prepare query
            $stmt = $db->prepare($query);
        
            // sanitize
            $userid=htmlspecialchars(strip_tags($userid));
            $templatename=htmlspecialchars(strip_tags($template_name));
            $previewimage=htmlspecialchars(strip_tags($image_url));
            $type=htmlspecialchars(strip_tags("email"));
            $clientname=htmlspecialchars(strip_tags($client_name));
            $project=htmlspecialchars(strip_tags($project_name));
            $smsbody=htmlspecialchars(strip_tags($email_body));
        
            // bind values
            $stmt->bindParam(":userid", $userid);
            $stmt->bindParam(":templatename", $templatename);
            $stmt->bindParam(":previewimage", $previewimage);
            $stmt->bindParam(":thumbnail", $previewimage);
            $stmt->bindParam(":type", $type);
            $stmt->bindParam(":clientname", $clientname);
            $stmt->bindParam(":project", $project);
            $stmt->bindParam(":smsbody", $smsbody);
            $stmt->execute();

        }else{

            $query = "INSERT INTO notification_templates SET userid=:userid, templatename=:templatename, thumbnail=:thumbnail, previewimage=:previewimage, type=:type, clientname=:clientname, project=:project, smsbody=:smsbody";    
            // prepare query
            $stmt = $db->prepare($query);
        
            // sanitize
            $userid=htmlspecialchars(strip_tags($userid));
            $templatename=htmlspecialchars(strip_tags($template_name));
            $previewimage=htmlspecialchars(strip_tags($image_url));
            $type=htmlspecialchars(strip_tags("email"));
            $clientname=htmlspecialchars(strip_tags($client_name));
            $project=htmlspecialchars(strip_tags($project_name));
            $smsbody=htmlspecialchars(strip_tags($email_body));
        
            // bind values
            $stmt->bindParam(":userid", $userid);
            $stmt->bindParam(":templatename", $templatename);
            $stmt->bindParam(":previewimage", $previewimage);
            $stmt->bindParam(":thumbnail", $previewimage);
            $stmt->bindParam(":type", $type);
            $stmt->bindParam(":clientname", $clientname);
            $stmt->bindParam(":project", $project);
            $stmt->bindParam(":smsbody", $smsbody);
            $stmt->execute();
        }
    }

    $sms_template = $data['sms_template'];
    foreach($sms_template as $rec1){
        $template_name = $rec1['template_name'];
        $sms_body = $rec1['sms_body'];

        $userid = $data['userid'];
        $query = "SELECT * FROM notification_templates WHERE userid = '".$userid."' AND templatename = '".$template_name."'";
        // prepare query statement
        $stmt = $db->prepare($query);
        // execute query
        $stmt->execute();
        
        if($stmt->rowCount() > 0){
            $query = "UPDATE notification_templates SET userid=:userid, type=:type, clientname=:clientname, project=:project, smsbody=:smsbody WHERE templatename=:templatename";    
            // prepare query
            $stmt = $db->prepare($query);
        
            // sanitize
            $userid=htmlspecialchars(strip_tags($userid));
            $templatename=htmlspecialchars(strip_tags($template_name));
            $type=htmlspecialchars(strip_tags("sms"));
            $clientname=htmlspecialchars(strip_tags($client_name));
            $project=htmlspecialchars(strip_tags($project_name));
            $smsbody=htmlspecialchars(strip_tags($sms_body));
        
            // bind values
            $stmt->bindParam(":userid", $userid);
            $stmt->bindParam(":templatename", $templatename);
            $stmt->bindParam(":type", $type);
            $stmt->bindParam(":clientname", $clientname);
            $stmt->bindParam(":project", $project);
            $stmt->bindParam(":smsbody", $sms_body);
            $stmt->execute();

        }else{

            $query = "INSERT INTO notification_templates SET userid=:userid, templatename=:templatename, type=:type, clientname=:clientname, project=:project, smsbody=:smsbody";    
            // prepare query
            $stmt = $db->prepare($query);
        
            // sanitize
            $userid=htmlspecialchars(strip_tags($userid));
            $templatename=htmlspecialchars(strip_tags($template_name));
            $previewimage=htmlspecialchars(strip_tags($image_url));
            $type=htmlspecialchars(strip_tags("sms"));
            $clientname=htmlspecialchars(strip_tags($client_name));
            $project=htmlspecialchars(strip_tags($project_name));
            $smsbody=htmlspecialchars(strip_tags($email_body));
        
            // bind values
            $stmt->bindParam(":userid", $userid);
            $stmt->bindParam(":templatename", $templatename);
            $stmt->bindParam(":type", $type);
            $stmt->bindParam(":clientname", $clientname);
            $stmt->bindParam(":project", $project);
            $stmt->bindParam(":smsbody", $smsbody);
            $stmt->execute();
        }
    }
    
    $sms_credit = $data['sms_credit'];
    $email_credit = $data['email_credit'];

    $userid = $data['userid'];
    $query = "SELECT * FROM credits WHERE userid = '".$userid."'";
    $stmt_q = $db->prepare($query);
    $stmt_q->execute();
    if($stmt_q->rowCount() > 0){
        $query = "UPDATE credits SET sms=:sms_credit, email=:email_credit WHERE userid=:userid";    
        $stmt = $db->prepare($query);
        
        // sanitize
        $userid=htmlspecialchars(strip_tags($userid));
        $sms_credit=htmlspecialchars(strip_tags($sms_credit));
        $email_credit=htmlspecialchars(strip_tags($email_credit));
        
        // bind values
        $stmt->bindParam(":userid", $userid);
        $stmt->bindParam(":sms_credit", $sms_credit);
        $stmt->bindParam(":email_credit", $email_credit);
        $stmt->execute();
    }else{
        $query = "INSERT INTO credits SET userid =:userid, sms=:sms_credit, email=:email_credit";    
        $stmt = $db->prepare($query);
        
        // sanitize
        $userid=htmlspecialchars(strip_tags($userid));
        $sms_credit=htmlspecialchars(strip_tags($sms_credit));
        $email_credit=htmlspecialchars(strip_tags($email_credit));
        
        // bind values
        $stmt->bindParam(":userid", $userid);
        $stmt->bindParam(":sms_credit", $sms_credit);
        $stmt->bindParam(":email_credit", $email_credit);
        $stmt->execute();
    }

    $user_arr=array(
        "status" => true,
        "error_code" => '1',
        "message" => "Successfully Signup!"
    );
}else{
    $user_arr=array(
        "status" => false,
        "error_code" => '-11',
        "message" => "Required parameter missing!",
    );
}

print_r(json_encode($user_arr));
?>