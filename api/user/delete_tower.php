<?php
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'); 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');

// get database connection
include_once '../config/database.php';
include_once '../objects/towers.php';

// instantiate user object
include_once '../objects/user.php';

$database = new Database();
$db = $database->getConnection();
 
$request_method = $_SERVER['REQUEST_METHOD'];
if($request_method != 'POST'){
    $user_arr=array(
        "status" => false,
        "error_code" => '0',
        "message" => $request_method. " method not available!"
    );
    print_r(json_encode($user_arr));
    exit;
}

$json = file_get_contents('php://input');
$data = json_decode($json, true);

if(!empty($data['id']))
{
    $id = $data['id'];
    /* $projectId = $data['project_id'];
    $typologyId = $data['typologyId']; */
    $status = "2";
    $datatime = date("Y-m-d H:i:s");

    $query = "UPDATE Towers SET `status`='".$status."' WHERE id = '".$id."'";
    $stmt = $db->prepare($query);
    $stmt->execute();
    
    $checkfloor = "SELECT id FROM ProjectTowerMappings WHERE towerId='".$id."'";
    $stmt_q = $db->prepare($checkfloor);
    $stmt_q->execute();                
    if($stmt_q->rowCount() > 0){
        $subinsert = "UPDATE ProjectTowerMappings SET `status`='".$status."' WHERE towerId='".$id."'";
        $stmt = $db->prepare($subinsert);
        $stmt->execute();
        
        $q = "DELETE FROM TowerFloorMappings WHERE towerId='".$id."'";
        $stmt = $db->prepare($q);
        $stmt->execute();
    }

    $user_arr=array(
        "status" => true,
        "error_code" => '1',
        "message" => "Tower deleted successfully!"
    );
    
}else{
    $user_arr=array(
        "status" => false,
        "error_code" => '-11',
        "message" => "Required parameter missing!",
    );
}

$db = null;
header('Content-Type: application/json');
print_r(json_encode($user_arr));
?>