<?php
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'); 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');

// get database connection
include_once '../config/database.php';
include_once '../objects/towers.php';

// instantiate user object
include_once '../objects/user.php';

$database = new Database();
$db = $database->getConnection();
 
$request_method = $_SERVER['REQUEST_METHOD'];
if($request_method != 'POST'){
    $user_arr=array(
        "status" => false,
        "error_code" => '0',
        "message" => $request_method. " method not available!"
    );
    print_r(json_encode($user_arr));
    exit;
}

$json = file_get_contents('php://input');
$data = json_decode($json, true);

if(!empty($data['unit']) && !empty($data['project_id']) && !empty($data['typologyId']) && !empty($data['flatName']) && !empty($data['price']) && ($data['soldStatus'] != ""))
{
    if(isset($data['id'])){
        $id = $data['id'];
    }else{
        $id = "";
    }

    $projectId = $data['project_id'];
    $towerId = $data['towerId'];
    $floorId = $data['floorId'];
    $typologyId = $data['typologyId'];
    $unit = $data['unit'];
    $flatName = $data['flatName'];
    $price = $data['price'];
    $soldStatus = $data['soldStatus'];
    if(isset($data['sqftSize'])){
        $sqftSize = $data['sqftSize'];
    }else{
        $sqftSize = "";
    }
    $status = '1';
    $datatime = date("Y-m-d H:i:s");

    if(empty($id)){

        $qry = "SELECT * FROM `Flats` WHERE `name`='".$flatName."'";
        $stmt = $db->prepare($qry);
        $stmt->execute();
        if($stmt->rowCount() > 0){
            $flat_list = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $flatId = $flat_list[0]['id'];
        }else{
            $query = "INSERT INTO Flats SET `name`='".$flatName."', `status`='".$status."', createdAt='".$datatime."', updatedAt='".$datatime."'";
            $stmt = $db->prepare($query);
            $stmt->execute();
            $flatId = $db->lastInsertId();
        }

        /* $query = "INSERT INTO Flats SET `name`='".$flatName."', `status`='".$status."', createdAt='".$datatime."', updatedAt='".$datatime."'";
        // prepare query
        $stmt = $db->prepare($query);
        $stmt->execute();
        $flatId = $db->lastInsertId();
        if($flatId > 0){    */         
            $qry = "SELECT id FROM TowerFloorMappings WHERE towerId = '".$towerId."' AND floorId = '".$floorId."'";
            $stmt_q = $db->prepare($qry);
            $stmt_q->execute();
            if($stmt_q->rowCount() > 0){
                $row = $stmt_q->fetchAll(PDO::FETCH_ASSOC);
                foreach($row as $val){
                    $towerFloorMappingId = $val['id'];
                    $subinsert = "INSERT INTO TowerFloorFlatMappings SET towerFloorMappingId='".$towerFloorMappingId."', `typologyId`='".$typologyId."', `soldStatus`='".$soldStatus."', flatId='".$flatId."', sqftSize='".$sqftSize."', unit='".$unit."', price='".$price."', createdAt='".$datatime."', updatedAt='".$datatime."'";
                    $stmt = $db->prepare($subinsert);
                    $stmt->execute();
                }
            }
        /* } */

    }else{
        //$flatId = $id;
        
        $qry = "SELECT * FROM `Flats` WHERE `name`='".$flatName."'";
        $stmt = $db->prepare($qry);
        $stmt->execute();
        if($stmt->rowCount() > 0){
            $flat_list = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $flatId = $flat_list[0]['id'];
        }else{
            $query = "INSERT INTO Flats SET `name`='".$flatName."', `status`='".$status."', createdAt='".$datatime."', updatedAt='".$datatime."'";
            $stmt = $db->prepare($query);
            $stmt->execute();
            $flatId = $db->lastInsertId();
        }

        $qry = "SELECT id FROM TowerFloorMappings WHERE towerId = '".$towerId."' AND floorId = '".$floorId."'";
        $stmt_q = $db->prepare($qry);
        $stmt_q->execute();
        if($stmt_q->rowCount() > 0){
            $row = $stmt_q->fetchAll(PDO::FETCH_ASSOC);
            foreach($row as $val){
                $towerFloorMappingId = $val['id'];
                $subinsert = "UPDATE TowerFloorFlatMappings SET `typologyId`='".$typologyId."', `soldStatus`='".$soldStatus."', flatId='".$flatId."', sqftSize='".$sqftSize."', unit='".$unit."', price='".$price."', updatedAt='".$datatime."' WHERE id = '".$id."'";
                $stmt = $db->prepare($subinsert);
                $stmt->execute();
            }
        }
        $flatId = "";
    }

    if(!empty($flatId)){
        $user_arr=array(
            "status" => true,
            "error_code" => '1',
            "message" => "Flat added successfully!"
        );
    }else{
        $user_arr=array(
            "status" => true,
            "error_code" => '1',
            "message" => "Flat updated successfully!"
        );
    }
}else{
    $user_arr=array(
        "status" => false,
        "error_code" => '-11',
        "message" => "Required parameter missing!",
    );
}

$db = null;
header('Content-Type: application/json');
print_r(json_encode($user_arr));
?>