<?php
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'); 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');

// get database connection
include_once '../config/database.php';
include_once '../objects/towers.php';

// instantiate user object
include_once '../objects/user.php';

$database = new Database();
$db = $database->getConnection();
 
$request_method = $_SERVER['REQUEST_METHOD'];
if($request_method != 'POST'){
    $user_arr=array(
        "status" => false,
        "error_code" => '0',
        "message" => $request_method. " method not available!"
    );
    print_r(json_encode($user_arr));
    exit;
}

$json = file_get_contents('php://input');
$data = json_decode($json, true);

if(!empty($data['project_id']))
{   
    $projectId = $data['project_id'];

    if(isset($data['towerId'])){
        $towerId = $data['towerId'];
    }else{
        $towerId = "";
    }

    if(isset($data['typologyId'])){
        $typologyId = $data['typologyId'];
    }else{
        $typologyId = "";
    }

    if(!empty($towerId) && !empty($typologyId)){
        $query = "SELECT id,projectId,`value`,priceSheetType,payRatio1,payRatio2,payRatio3,payRatio4,payRatio5,payerRatio1,payerRatio2,payerRatio3,payerRatio4,payerRatio5,towerId,typologyId,towerFloorFlatMappingId,price, (SELECT unit FROM TowerFloorFlatMappings WHERE id = towerFloorFlatMappingId LIMIT 1) as unit FROM Subventions WHERE projectId='".$projectId."' AND priceSheetType = '1' AND towerId = '".$towerId."' AND typologyId = '".$typologyId."'";
    }else if(empty($towerId) && !empty($typologyId)){
        $query = "SELECT id,projectId,`value`,priceSheetType,payRatio1,payRatio2,payRatio3,payRatio4,payRatio5,payerRatio1,payerRatio2,payerRatio3,payerRatio4,payerRatio5,towerId,typologyId,towerFloorFlatMappingId,price, (SELECT unit FROM TowerFloorFlatMappings WHERE id = towerFloorFlatMappingId LIMIT 1) as unit FROM Subventions WHERE projectId='".$projectId."' AND priceSheetType = '1' AND typologyId = '".$typologyId."'";
    }else if(!empty($towerId) && empty($typologyId)){
        $query = "SELECT id,projectId,`value`,priceSheetType,payRatio1,payRatio2,payRatio3,payRatio4,payRatio5,payerRatio1,payerRatio2,payerRatio3,payerRatio4,payerRatio5,towerId,typologyId,towerFloorFlatMappingId,price, (SELECT unit FROM TowerFloorFlatMappings WHERE id = towerFloorFlatMappingId LIMIT 1) as unit FROM Subventions WHERE projectId='".$projectId."' AND priceSheetType = '1' AND towerId = '".$towerId."'";
    }else{
        $query = "SELECT id,projectId,`value`,priceSheetType,payRatio1,payRatio2,payRatio3,payRatio4,payRatio5,payerRatio1,payerRatio2,payerRatio3,payerRatio4,payerRatio5,towerId,typologyId,towerFloorFlatMappingId,price, (SELECT unit FROM TowerFloorFlatMappings WHERE id = towerFloorFlatMappingId LIMIT 1) as unit FROM Subventions WHERE projectId='".$projectId."' AND priceSheetType = '1'";
    }
    // prepare query statement
    $stmt = $db->prepare($query);
    // execute query
    $stmt->execute();
    if($stmt->rowCount() > 0){
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $response = array();
        foreach($row as $val){
            $d['id'] = $val['id'];
            $d['projectId'] = $val['projectId'];
            $d['value'] = $val['value'];
            $d['priceSheetType'] = $val['priceSheetType'];
            $d['towerId'] = $val['towerId'];
            $d['typologyId'] = $val['typologyId'];
            $d['towerFloorFlatMappingId'] = $val['towerFloorFlatMappingId'];
            $d['price'] = $val['price'];
            $d['unit'] = $val['unit'];

            $fields = array();
            for ($x = 1; $x <= 5; $x++) {
                if( ( $val['payRatio'.$x] == 'NULL' || $val['payRatio'.$x] == NULL || $val['payRatio'.$x] == "0" ) && ( $val['payerRatio'.$x] == 'NULL' || $val['payerRatio'.$x] == NULL ) ){}else{
                    $r['payRatio'] = $val['payRatio'.$x];
                    $r['payerRatio'] = $val['payerRatio'.$x];
                    $fields[] = $r;
                }
            }
            
            $d['fields'] = $fields;
            $response[] = $d;
        }

        $user_arr=array(
            "status" => true,
            "error_code" => '1',
            "message" => "list of Subventions",
            "data" => $response
        );
    }else{
        $user_arr=array(
            "status" => true,
            "error_code" => '0',
            "message" => "Recort not found!"
        );
    }
}else{
    $user_arr=array(
        "status" => false,
        "error_code" => '-11',
        "message" => "Required parameter missing!",
    );
}

$db = null;
header('Content-Type: application/json');
print_r(json_encode($user_arr));
?>