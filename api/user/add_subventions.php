<?php
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'); 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');

// get database connection
include_once '../config/database.php';
include_once '../objects/towers.php';

// instantiate user object
include_once '../objects/user.php';

$database = new Database();
$db = $database->getConnection();
 
$request_method = $_SERVER['REQUEST_METHOD'];
if($request_method != 'POST'){
    $user_arr=array(
        "status" => false,
        "error_code" => '0',
        "message" => $request_method. " method not available!"
    );
    print_r(json_encode($user_arr));
    exit;
}

$json = file_get_contents('php://input');
$data = json_decode($json, true);

if(!empty($data['project_id']) && !empty($data['fields']) && !empty($data['priceSheetType']) && !empty($data['towerId']) && !empty($data['typologyId']) && !empty($data['price']))
{
    if(isset($data['id']) && !empty($data['id'])){
        $id = $data['id'];
    }else{
        $id = "";
    }

    if(isset($data['towerId']) && !empty($data['towerId'])){
        $towerId = $data['towerId'];
    }else{
        $towerId = "";
    }

    if(isset($data['typologyId']) && !empty($data['typologyId'])){
        $typologyId = $data['typologyId'];
    }else{
        $typologyId = "";
    }

    if(isset($data['towerFloorFlatMappingId']) && !empty($data['towerFloorFlatMappingId'])){
        $towerFloorFlatMappingId = $data['towerFloorFlatMappingId'];
    }else{
        $towerFloorFlatMappingId = "0";
    }
    
    if(isset($data['price']) && !empty($data['price'])){
        $price = $data['price'];
    }else{
        $price = "";
    }

    $projectId = $data['project_id'];
    $priceSheetType = $data['priceSheetType'];
    $fields = $data['fields'];
    $tot_fields = count($fields);

    if($tot_fields > 0){
        $where = '';
        $i = 1;
        $payRatio = array();
        foreach($fields as $val){

            $payRatio[] = $val['payRatio'];

            $where .= 'payRatio'.$i. '="' . $val['payRatio'].'", payerRatio'.$i.'="'. $val['payerRatio'].'", ';   
            $i++;
        }

        $value = implode(":",$payRatio);

        if($tot_fields < 5){
            for ($x = $tot_fields+1; $x <= 5; $x++) {
                $where .= 'payRatio'.$x. '= "NULL", payerRatio'.$x.'= "NULL", ';
            }
        }
        $where = rtrim($where, ", ");
    }else{
        $where = '';
        $tot_fields = 0;
        for ($x = $tot_fields; $x < 5; $x++) {
            $where .= 'payRatio'.$i. '= "NULL", payerRatio'.$i.'= "NULL", ';
        }
        $where = rtrim($where, ", ");
        $value = "NULL";
    }
    
    /* 
    Check unique v
    towerId
    typologyId
    value */

    if(empty($data['id']))
    {
        if(isset($data['towerFloorFlatMappingId']) && !empty($data['towerFloorFlatMappingId'])){
            if(isset($data['id']) && !empty($data['id'])){
                $query = "SELECT id FROM Subventions WHERE projectId='".$projectId."' AND towerId = '".$towerId."' AND typologyId = '".$typologyId."' AND `value` = '".$value."' AND towerFloorFlatMappingId = '".$towerFloorFlatMappingId."' AND id != '".$id."'";
            }else{
                $query = "SELECT id FROM Subventions WHERE projectId='".$projectId."' AND towerId = '".$towerId."' AND typologyId = '".$typologyId."' AND `value` = '".$value."' AND towerFloorFlatMappingId = '".$towerFloorFlatMappingId."'";
            }
        }else{
            if(isset($data['id']) && !empty($data['id'])){
                $query = "SELECT id FROM Subventions WHERE projectId='".$projectId."' AND towerId = '".$towerId."' AND typologyId = '".$typologyId."' AND `value` = '".$value."' AND id != '".$id."'";
            }else{
                $query = "SELECT id FROM Subventions WHERE projectId='".$projectId."' AND towerId = '".$towerId."' AND typologyId = '".$typologyId."' AND `value` = '".$value."'";
            }
        }
    

        // prepare query statement
        $stmt = $db->prepare($query);
        // execute query
        $stmt->execute();
        if($stmt->rowCount() > 0){
            $user_arr=array(
                "status" => false,
                "error_code" => '0',
                "message" => "Subventions details already added!"
            );
            $db = null;
            header('Content-Type: application/json');
            print_r(json_encode($user_arr));
            exit;
        }
    }

    $datatime = date("Y-m-d H:i:s");    
    if(empty($id)){

        $query = "INSERT INTO Subventions SET `projectId`='".$projectId."', `priceSheetType`='".$priceSheetType."', `value`='".$value."', `towerId`='".$towerId."', `typologyId`='".$typologyId."', `towerFloorFlatMappingId`='".$towerFloorFlatMappingId."', `price`='".$price."', $where, createdAt='".$datatime."', updatedAt='".$datatime."'";
        // prepare query
        $stmt = $db->prepare($query);
        $stmt->execute();
        $subventionId = $db->lastInsertId();
        
        $query = "INSERT INTO PaymentMilestoneMappings SET `subventionId`='".$subventionId."', `mileStoneId`='1', `percentage`='5', `status`='1', createdAt='".$datatime."', updatedAt='".$datatime."'";
        $stmt = $db->prepare($query);
        $stmt->execute();
        
        $query = "INSERT INTO PaymentMilestoneMappings SET `subventionId`='".$subventionId."', `mileStoneId`='2', `percentage`='5', `status`='1', createdAt='".$datatime."', updatedAt='".$datatime."'";
        $stmt = $db->prepare($query);
        $stmt->execute();

        $query = "INSERT INTO PaymentMilestoneMappings SET `subventionId`='".$subventionId."', `mileStoneId`='3', `percentage`='10', `status`='1', createdAt='".$datatime."', updatedAt='".$datatime."'";
        $stmt = $db->prepare($query);
        $stmt->execute();

        $query = "INSERT INTO PaymentMilestoneMappings SET `subventionId`='".$subventionId."', `mileStoneId`='4', `percentage`='5', `status`='1', createdAt='".$datatime."', updatedAt='".$datatime."'";
        $stmt = $db->prepare($query);
        $stmt->execute();
        
        $query = "INSERT INTO PaymentMilestoneMappings SET `subventionId`='".$subventionId."', `mileStoneId`='5', `percentage`='5', `status`='1', createdAt='".$datatime."', updatedAt='".$datatime."'";
        $stmt = $db->prepare($query);
        $stmt->execute();

        $query = "INSERT INTO PaymentMilestoneMappings SET `subventionId`='".$subventionId."', `mileStoneId`='6', `percentage`='5', `status`='1', createdAt='".$datatime."', updatedAt='".$datatime."'";
        $stmt = $db->prepare($query);
        $stmt->execute();

        $query = "INSERT INTO PaymentMilestoneMappings SET `subventionId`='".$subventionId."', `mileStoneId`='7', `percentage`='5', `status`='1', createdAt='".$datatime."', updatedAt='".$datatime."'";
        $stmt = $db->prepare($query);
        $stmt->execute();

        $query = "INSERT INTO PaymentMilestoneMappings SET `subventionId`='".$subventionId."', `mileStoneId`='8', `percentage`='6', `status`='1', createdAt='".$datatime."', updatedAt='".$datatime."'";
        $stmt = $db->prepare($query);
        $stmt->execute();

        $query = "INSERT INTO PaymentMilestoneMappings SET `subventionId`='".$subventionId."', `mileStoneId`='9', `percentage`='6', `status`='1', createdAt='".$datatime."', updatedAt='".$datatime."'";
        $stmt = $db->prepare($query);
        $stmt->execute();

        $query = "INSERT INTO PaymentMilestoneMappings SET `subventionId`='".$subventionId."', `mileStoneId`='10', `percentage`='6', `status`='1', createdAt='".$datatime."', updatedAt='".$datatime."'";
        $stmt = $db->prepare($query);
        $stmt->execute();

        $query = "INSERT INTO PaymentMilestoneMappings SET `subventionId`='".$subventionId."', `mileStoneId`='11', `percentage`='6', `status`='1', createdAt='".$datatime."', updatedAt='".$datatime."'";
        $stmt = $db->prepare($query);
        $stmt->execute();

        $query = "INSERT INTO PaymentMilestoneMappings SET `subventionId`='".$subventionId."', `mileStoneId`='12', `percentage`='6', `status`='1', createdAt='".$datatime."', updatedAt='".$datatime."'";
        $stmt = $db->prepare($query);
        $stmt->execute();

        $query = "INSERT INTO PaymentMilestoneMappings SET `subventionId`='".$subventionId."', `mileStoneId`='13', `percentage`='5', `status`='1', createdAt='".$datatime."', updatedAt='".$datatime."'";
        $stmt = $db->prepare($query);
        $stmt->execute();

        $query = "INSERT INTO PaymentMilestoneMappings SET `subventionId`='".$subventionId."', `mileStoneId`='14', `percentage`='5', `status`='1', createdAt='".$datatime."', updatedAt='".$datatime."'";
        $stmt = $db->prepare($query);
        $stmt->execute();

        $query = "INSERT INTO PaymentMilestoneMappings SET `subventionId`='".$subventionId."', `mileStoneId`='15', `percentage`='5', `status`='1', createdAt='".$datatime."', updatedAt='".$datatime."'";
        $stmt = $db->prepare($query);
        $stmt->execute();

        $query = "INSERT INTO PaymentMilestoneMappings SET `subventionId`='".$subventionId."', `mileStoneId`='16', `percentage`='10', `status`='1', createdAt='".$datatime."', updatedAt='".$datatime."'";
        $stmt = $db->prepare($query);
        $stmt->execute();

        $query = "INSERT INTO PaymentMilestoneMappings SET `subventionId`='".$subventionId."', `mileStoneId`='17', `percentage`='5', `status`='1', createdAt='".$datatime."', updatedAt='".$datatime."'";
        $stmt = $db->prepare($query);
        $stmt->execute();
    }else{
        $query = "UPDATE Subventions SET `projectId`='".$projectId."', `value`='".$value."', `priceSheetType`='".$priceSheetType."', `towerId`='".$towerId."', `typologyId`='".$typologyId."', `towerFloorFlatMappingId`='".$towerFloorFlatMappingId."', `price`='".$price."', $where, updatedAt='".$datatime."' WHERE id = '".$id."'";
        // prepare query
        $stmt = $db->prepare($query);
        $stmt->execute();
        $subventionId = $id;
    }

    if(empty($id)){
        $user_arr=array(
            "status" => true,
            "error_code" => '1',
            "message" => "Subventions added successfully!"
        );
    }else{
        $user_arr=array(
            "status" => true,
            "error_code" => '1',
            "message" => "Subventions updated successfully!"
        );
    }
}else{
    $user_arr=array(
        "status" => false,
        "error_code" => '-11',
        "message" => "Required parameter missing!",
    );
}

$db = null;
header('Content-Type: application/json');
print_r(json_encode($user_arr));
?>