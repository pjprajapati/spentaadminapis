<?php
class Config{
 
    // database connection and table name
    private $conn;
    private $table_name = "configpersona";
 
    // object properties
    public $id;
    public $type;
    public $funnel_plus;
    public $connect_plus;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
    
    // signup user
    function dataInsert(){
    
        if($this->isAlreadyExist()){
            return false;
        }
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                type=:type, funnel_plus=:funnel_plus, connect_plus=:connect_plus, createddate=:createddate";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->type=htmlspecialchars(strip_tags($this->type));
        $this->funnel_plus=htmlspecialchars(strip_tags($this->funnel_plus));
        $this->connect_plus=htmlspecialchars(strip_tags($this->connect_plus));
        $this->createddate= date("Y-m-d H:i:s");
    
        // bind values
        $stmt->bindParam(":type", $this->type);
        $stmt->bindParam(":funnel_plus", $this->funnel_plus);
        $stmt->bindParam(":connect_plus", $this->connect_plus);
        $stmt->bindParam(":createddate", $this->createddate);
    
        // execute query
        if($stmt->execute()){
            $this->id = $this->conn->lastInsertId();
            return true;
        }
    
        return false;
        
    }

    // config data
    function getDataByType(){
        // select all query
        $query = "SELECT
                    `id`, `type`, `funnel_plus`, `connect_plus` , `createddate`
                FROM
                    " . $this->table_name . " 
                WHERE
                    type='".$this->type."'";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }

    // config data
    function getDataById(){
        // select all query
        $query = "SELECT
                    `id`, `type`, `funnel_plus`, `connect_plus` , `createddate`
                FROM
                    " . $this->table_name . " 
                WHERE
                    id='".$this->id."'";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }

    // config data
    function getAllData(){
        // select all query
        $query = "SELECT
                    `id`, `type`, `funnel_plus`, `connect_plus` , `createddate`
                FROM
                    " . $this->table_name;
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }

    // config data
    function getColumnsName(){
        // select all query
        $query = "SELECT
                    `COLUMN_NAME`
                FROM
                `INFORMATION_SCHEMA`.`COLUMNS`
                WHERE
                `TABLE_SCHEMA`='insightsdb' 
                AND `TABLE_NAME`='leads'";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }
}