<?php
class User{
 
    // database connection and table name
    private $conn;
    private $table_name = "admin_user";
 
    // object properties
    public $id;
    public $email;
    public $username;
    public $project_id;
    public $password;
    public $created_on;
    
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // signup user
    function signup(){
    
        if($this->isAlreadyExist()){
            return false;
        }

        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    userid=:userid, password=:password, firstname=:firstname, lastname=:lastname, usertype=:usertype, project=:project, config=:config, sector=:configtype, client_name=:client_name";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->userid=strip_tags($this->userid);
        $this->password=strip_tags($this->password);
        $this->firstname=strip_tags($this->firstname);
        $this->lastname=strip_tags($this->lastname);
        $this->usertype=strip_tags($this->usertype);
        $this->project=strip_tags($this->project);
        $this->config=$this->config;
        
        // bind values
        $stmt->bindParam(":userid", $this->userid);
        $stmt->bindParam(":password", $this->password);
        $stmt->bindParam(":firstname", $this->firstname);
        $stmt->bindParam(":lastname", $this->lastname);
        $stmt->bindParam(":usertype", $this->usertype);
        $stmt->bindParam(":project", $this->project);
        $stmt->bindParam(":config", $this->config);
        $stmt->bindParam(":configtype", $this->configtype);
        $stmt->bindParam(":client_name", $this->client_name);        
        
        // execute query
        if($stmt->execute()){
            $this->id = $this->conn->lastInsertId();
            return true;
        }
        return false;
    }
    
    function userUpdate(){
        // query to insert record
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    firstname=:firstname, lastname=:lastname, usertype=:usertype, project=:project, config=:config, sector=:configtype, client_name=:client_name
                WHERE userid=:userid";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->userid=strip_tags($this->userid);
        $this->password=strip_tags($this->password);
        $this->firstname=strip_tags($this->firstname);
        $this->lastname=strip_tags($this->lastname);
        $this->usertype=strip_tags($this->usertype);
        $this->project=strip_tags($this->project);
        $this->config=$this->config;
    
        // bind values
        $stmt->bindParam(":userid", $this->userid);
        //$stmt->bindParam(":password", $this->password);
        $stmt->bindParam(":firstname", $this->firstname);
        $stmt->bindParam(":lastname", $this->lastname);
        $stmt->bindParam(":usertype", $this->usertype);
        $stmt->bindParam(":project", $this->project);
        $stmt->bindParam(":config", $this->config);
        $stmt->bindParam(":configtype", $this->configtype);
        $stmt->bindParam(":client_name", $this->client_name);
        
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }
    
    // login user
    function login(){
        // select all query
        $query = "SELECT id,project_id,username,email,created_on FROM " . $this->table_name . " WHERE email='".$this->email."' AND password='".md5($this->password)."'";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }

    // login user
    function getUserDetails(){
        // select all query
        $query = "SELECT
                   *
                FROM
                    " . $this->table_name . " 
                WHERE
                userid='".$this->userid."'";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }

    function isAlreadyExist(){
        $query = "SELECT *
            FROM
                " . $this->table_name . " 
            WHERE
            userid='".$this->userid."'";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        if($stmt->rowCount() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    // config data
    function getAllData(){
        
        if(!empty($this->userid) || !empty($this->client_name)){
            $query = "SELECT * FROM " . $this->table_name." WHERE (userid='".$this->userid."' OR client_name LIKE '%".$this->client_name."%' OR created_on BETWEEN '".$this->startdate."' AND '".$this->enddate."')";
        }else{
            $query = "SELECT
                    *
                FROM
                    " . $this->table_name;
        }
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }

    // config data
    function getAllDataWithPaging(){
        
        if(!empty($this->userid) || !empty($this->client_name)){
            $query = "SELECT * FROM " . $this->table_name." WHERE (userid='".$this->userid."' OR client_name LIKE '%".$this->client_name."%' OR created_on BETWEEN '".$this->startdate."' AND '".$this->enddate."') LIMIT ". $this->startPage.",".$this->page_rows;
        }else{
            $query = "SELECT * FROM " . $this->table_name." LIMIT ". $this->startPage.",".$this->page_rows;
        }
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    // login user
    function getTempletes(){
        // select all query
        $query = "SELECT
                   *
                FROM
                notification_templates 
                WHERE
                userid='".$this->userid."' AND type = '".$this->type."'";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }

    // signup user
    function insertTemp(){
    
        if($this->isAlreadyExist()){
            return false;
        }

        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    userid=:userid, password=:password, firstname=:firstname, lastname=:lastname, usertype=:usertype, project=:project, config=:config";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->userid=htmlspecialchars(strip_tags($this->userid));
        $this->password=htmlspecialchars(strip_tags($this->password));
        $this->firstname=htmlspecialchars(strip_tags($this->firstname));
        $this->lastname=htmlspecialchars(strip_tags($this->lastname));
        $this->usertype=htmlspecialchars(strip_tags($this->usertype));
        $this->project=htmlspecialchars(strip_tags($this->project));
        $this->config=htmlspecialchars(strip_tags($this->config));
    
        // bind values
        $stmt->bindParam(":userid", $this->userid);
        $stmt->bindParam(":password", $this->password);
        $stmt->bindParam(":firstname", $this->firstname);
        $stmt->bindParam(":lastname", $this->lastname);
        $stmt->bindParam(":usertype", $this->usertype);
        $stmt->bindParam(":project", $this->project);
        $stmt->bindParam(":config", $this->config);
    
        // execute query
        if($stmt->execute()){
            $this->id = $this->conn->lastInsertId();
            return true;
        }
    
        return false;
        
    }
}