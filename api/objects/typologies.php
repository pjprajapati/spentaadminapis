<?php
class Typologies{
    
    // database connection and table name
    private $conn;
    private $table_name = "Typologies";
    private $sub_table_name = "CustomerProjectTypologyMappings";
    
    // object properties
    public $id;
    public $project_id;
    public $name;
    public $color;
    public $status;
    public $customField1;
    public $customerId;
    public $towerId;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
    
    // config data
    function getDataById(){
        // select all query
        $query = "SELECT `id`, `name`, `image`, `preBookingAmount` FROM " . $this->table_name . " WHERE id='".$this->id."'";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }

    function getDataByProjectId(){

        if(isset($this->towerId) && !empty($this->towerId)){
            $query = "SELECT t.id, t.name, t.color, t.status, t.customField1, t1.projectid as project_id FROM " . $this->table_name . " as t 
            INNER JOIN " . $this->sub_table_name . " as t1 ON t1.typologyId = t.id 
            LEFT JOIN TowerFloorFlatMappings as tffm ON t1.typologyId = tffm.typologyId
            LEFT JOIN TowerFloorMappings as tfm ON tffm.towerFloorMappingId = tfm.id 
            WHERE t1.projectid='".$this->project_id."' AND t.status = '1' AND tfm.towerId = '".$this->towerId."' GROUP BY t.id";
        }else{
            // select all query
            $query = "SELECT t.id, t.name, t.color, t.status, t.customField1, t1.projectid as project_id FROM " . $this->table_name . " as t INNER JOIN " . $this->sub_table_name . " as t1 ON t1.typologyId = t.id WHERE t1.projectid='".$this->project_id."' AND t.status = '1'";
        }
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }

    // config data
    function getAllData(){
        // select all query
        $query = "SELECT
                    `id`, `type`, `funnel_plus`, `connect_plus` , `createddate`
                FROM
                    " . $this->table_name;
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }

    // signup user
    function insert(){
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    name=:name, color=:color, status=:status, customField1=:customField1, createdAt=:createdAt, updatedAt=:updatedAt";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        $createdAt = date("Y-m-d H:i:s");
        // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":color", $this->color);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":customField1", $this->customField1);
        $stmt->bindParam(":createdAt", $createdAt);
        $stmt->bindParam(":updatedAt", $createdAt);    
        
        // execute query
        if($stmt->execute()){
            $this->id = $this->conn->lastInsertId();
            return $this->id;
        }
        return false;
    }

    function update(){
        // query to insert record
        $query = "UPDATE " . $this->table_name . " SET name=:name, color=:color, status=:status, customField1=:customField1, updatedAt=:updatedAt
                WHERE id=:id";
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        $createdAt = date("Y-m-d H:i:s");
        // bind values
        $stmt->bindParam(":id", $this->id);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":color", $this->color);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":customField1", $this->customField1);
        $stmt->bindParam(":updatedAt", $createdAt);
        
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    // signup user
    function insertsub(){
        // query to insert record
        $query = "INSERT INTO
                    " . $this->sub_table_name . "
                SET projectId=:projectId, customerId=:customerId, typologyId=:typologyId, status=:status, createdAt=:createdAt, updatedAt=:updatedAt";
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        $createdAt = date("Y-m-d H:i:s");
        // bind values
        $stmt->bindParam(":projectId", $this->project_id);
        $stmt->bindParam(":customerId", $this->customerId);
        $stmt->bindParam(":typologyId", $this->id);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":createdAt", $createdAt);
        $stmt->bindParam(":updatedAt", $createdAt);
        
        // execute query
        if($stmt->execute()){
            $this->id = $this->conn->lastInsertId();
            return $this->id;
        }
        return false;
    }

    function updatesub(){
        // query to insert record
        $query = "UPDATE " . $this->sub_table_name . " SET projectId=:projectId, customerId=:customerId, status=:status, updatedAt=:updatedAt WHERE typologyId=:typologyId";
        // prepare query
        $stmt = $this->conn->prepare($query);
        
        $createdAt = date("Y-m-d H:i:s");
        // bind values
        $stmt->bindParam(":typologyId", $this->id);
        $stmt->bindParam(":customerId", $this->customerId);
        $stmt->bindParam(":projectId", $this->project_id);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":updatedAt", $createdAt);
        
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    function delete(){
        // query to insert record
        $query = "DELETE FROM " . $this->table_name . " WHERE id='".$this->id."'";
        // prepare query
        $stmt = $this->conn->prepare($query);
        
        $createdAt = date("Y-m-d H:i:s");
        // bind values
        //$stmt->bindParam(":typologyId", $this->id);
        
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    function deletesub(){
        // query to insert record
        $query = "DELETE FROM " . $this->sub_table_name . " WHERE typologyId='".$this->id."'";
        // prepare query
        $stmt = $this->conn->prepare($query);
        
        $createdAt = date("Y-m-d H:i:s");
        // bind values
        $stmt->bindParam(":typologyId", $this->id);
        
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }
}