<?php
class Towers{
    
    // database connection and table name
    private $conn;
    private $table_name = "Towers";
    private $sub_table_name = "ProjectTowerMappings";
    
    // object properties
    public $id;
    public $project_id;
    public $name;
    public $color;
    public $status;
    public $customField1;
    public $customerId;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
    
    // config data
    function getDataById(){
        // select all query
        $query = "SELECT `id`, `name`, `image`, `preBookingAmount` FROM " . $this->table_name . " WHERE id='".$this->id."'";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }

    function getDataByProjectId(){
        // select all query
        $query = "SELECT t.id, t.name, t.image, t.images, t.status, t.points, t.totalFloors, t1.projectid as project_id, t1.sqftSize, t1.sqmtSize, t1.reraNumber, t.ticketSizeMin, t.ticketSizeMax FROM " . $this->table_name . " as t INNER JOIN " . $this->sub_table_name . " as t1 ON t1.towerId = t.id WHERE t1.projectid='".$this->project_id."' AND t.status = '1' GROUP BY t1.towerId";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }

    // config data
    function getAllData(){
        // select all query
        $query = "SELECT
                    `id`, `type`, `funnel_plus`, `connect_plus` , `createddate`
                FROM
                    " . $this->table_name;
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }

    // signup user
    function insert(){
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    name=:name, status=:status, points=:points, image=:image, createdAt=:createdAt, updatedAt=:updatedAt";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        $createdAt = date("Y-m-d H:i:s");
        // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":points", $this->points);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":image", $this->image);
        $stmt->bindParam(":createdAt", $createdAt);
        $stmt->bindParam(":updatedAt", $createdAt);    
        
        // execute query
        if($stmt->execute()){
            $this->id = $this->conn->lastInsertId();
            return $this->id;
        }
        return false;
    }

    function update(){
        // query to insert record
        $query = "UPDATE " . $this->table_name . " SET name=:name, status=:status, points=:points, image=:image, updatedAt=:updatedAt
                WHERE id=:id";
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        $createdAt = date("Y-m-d H:i:s");
        // bind values
        $stmt->bindParam(":id", $this->id);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":points", $this->points);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":image", $this->image);
        $stmt->bindParam(":updatedAt", $createdAt);
        
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    // signup user
    function insertsub(){
        // query to insert record
        $query = "INSERT INTO
                    " . $this->sub_table_name . "
                SET projectId=:projectId, towerId=:towerId, typologyId=:typologyId, status=:status, image=:image, sqftSize=:sqftSize, sqmtSize=:sqmtSize, reraNumber=:reraNumber, createdAt=:createdAt, updatedAt=:updatedAt";
        // prepare query
        $stmt = $this->conn->prepare($query);
        
        $createdAt = date("Y-m-d H:i:s");
        // bind values
        $stmt->bindParam(":projectId", $this->project_id);
        $stmt->bindParam(":towerId", $this->id);
        $stmt->bindParam(":typologyId", $this->typologyId);
        $stmt->bindParam(":image", $this->image);
        $stmt->bindParam(":sqftSize", $this->sqftSize);
        $stmt->bindParam(":sqmtSize", $this->sqmtSize);
        $stmt->bindParam(":reraNumber", $this->reraNumber);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":createdAt", $createdAt);
        $stmt->bindParam(":updatedAt", $createdAt);
        
        // execute query
        if($stmt->execute()){
            $this->id = $this->conn->lastInsertId();
            return $this->id;
        }
        return false;
    }

    function updatesub(){
        // query to insert record
        $query = "UPDATE " . $this->sub_table_name . " SET projectId=:projectId, customerId=:customerId, status=:status, updatedAt=:updatedAt WHERE typologyId=:typologyId";
        // prepare query
        $stmt = $this->conn->prepare($query);
        
        $createdAt = date("Y-m-d H:i:s");
        // bind values
        $stmt->bindParam(":typologyId", $this->id);
        $stmt->bindParam(":customerId", $this->customerId);
        $stmt->bindParam(":projectId", $this->project_id);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":updatedAt", $createdAt);
        
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    function delete(){
        // query to insert record
        $query = "DELETE FROM " . $this->table_name . " WHERE id='".$this->id."'";
        // prepare query
        $stmt = $this->conn->prepare($query);
        
        $createdAt = date("Y-m-d H:i:s");
        // bind values
        //$stmt->bindParam(":typologyId", $this->id);
        
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    function deletesub(){
        // query to insert record
        $query = "DELETE FROM " . $this->sub_table_name . " WHERE typologyId='".$this->id."'";
        // prepare query
        $stmt = $this->conn->prepare($query);
        
        $createdAt = date("Y-m-d H:i:s");
        // bind values
        $stmt->bindParam(":typologyId", $this->id);
        
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }
}