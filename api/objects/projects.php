<?php
class Projects{
 
    // database connection and table name
    private $conn;
    private $table_name = "Projects";
 
    // object properties
    public $id;
    public $project_id;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
    
    // config data
    function getDataById(){
        // select all query
        $query = "SELECT `id`, `name`, `images`, `image`, `preBookingAmount` FROM " . $this->table_name . " WHERE id='".$this->project_id."'";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
        return $stmt;
    }
}